package questao9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class BinaryTree {
	  int value;
	  BinaryTree left, right;

	  public BinaryTree(int value, BinaryTree left, BinaryTree right) {
	    this.value = value;
	    this.left = left;
	    this.right = right;
	  }

	  public BinaryTree left() {
	    return this.left;
	  }

	  public BinaryTree right() {
	    return this.right;
	  }

	  @SuppressWarnings("resource")
	  private void excutar() {
		List<Integer> valoresNos = new ArrayList<>();
		int  somaDosNos =0;
	    Stack<BinaryTree> nodeStack = new Stack<BinaryTree>();
	    BinaryTree currentNode = this;
	    nodeStack.push(currentNode);
	    Scanner reader = new Scanner(System.in);
		System.out.println ("Digite o valor do n� a ser buscado");
		int findValue = reader.nextInt();
	    preOrder(currentNode,findValue, valoresNos);
	    for (Integer valor : valoresNos) {
			somaDosNos = somaDosNos + valor;
		}
	    System.out.println("A soma dos n�s subsequentes � " + (somaDosNos - findValue));
	  }
	  
	  public void preOrder(BinaryTree currentNode, int findValue, List<Integer> valoresNos) {
		  
		  if(currentNode != null) {
			  if(currentNode.value == findValue) {
				  System.out.println("n� encontrado");
				  getValores(currentNode, valoresNos);
				  return;
			  }
			  preOrder(currentNode.left,findValue, valoresNos);
			  preOrder(currentNode.right, findValue, valoresNos);
		  }
	  }

	  private void getValores(BinaryTree currentNode, List<Integer> valoresNos) {
		  if(currentNode != null) {
			  valoresNos.add(currentNode.value);
			  getValores(currentNode.left, valoresNos);
			  getValores(currentNode.right, valoresNos);
		  }
	  }

	public static void main(String args[]) {
	    BinaryTree tree = new BinaryTree(6, null, new BinaryTree(7,new BinaryTree(10,null,new BinaryTree(12,null,null)),null)); 
	    tree.excutar();
	  }

	}
